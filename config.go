package srv

import (
	"bytes"
	"encoding/json"
	"io/ioutil"
	"text/template"
)

// Config is the unmarshalled config.json file.
type Config struct {
	Addsite struct {
		VirtualHostDir string
		SiteDir        string
	}
}

// UnmarshalConfig preforms the unmarshalling of config.json files.
func UnmarshalConfig(b []byte) (*Config, error) {
	var conf Config

	err := json.Unmarshal(b, &conf)
	if err != nil {
		return nil, err
	}

	return &conf, nil
}

// GetConfig reads the config.json file from the path argument and returns the
// unmarshalled result.
func GetConfig(path string) (*Config, error) {
	b, err := ioutil.ReadFile(path)
	if err != nil {
		return nil, err
	}

	return UnmarshalConfig(b)
}

// GetConfigValue will use go templates to parse a config value using the data
// argument.
func GetConfigValue(value string, data interface{}) (string, error) {
	var b bytes.Buffer
	tmpl, err := template.New("").Parse(value)
	if err != nil {
		return "", err
	}

	err = tmpl.Execute(&b, data)
	if err != nil {
		return "", err
	}

	return b.String(), nil
}
