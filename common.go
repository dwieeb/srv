package srv

import (
	"errors"
	"flag"
	"fmt"
	"os"
)

// ErrArgMismatch occurs when the number of arguments supplied does not equal
// the number of arguments expected.
var ErrArgMismatch = errors.New("argument mismatch")

// ParseFlags parses command line args from os.Args using the flags argument.
// It preforms a check on the number of parsed args, printing the flagset's
// usage and returning ErrArgMismatch if the number is unexpected.
func ParseFlags(flags *flag.FlagSet, expectedNArg int) error {
	flags.Parse(os.Args[1:])

	if flags.NArg() != expectedNArg {
		fmt.Println("error: " + ErrArgMismatch.Error() + "\n")
		flags.Usage()
		return ErrArgMismatch
	}

	return nil
}

// ParseConfig parses a config file from the path argument. If an error is
// encountered, it is printed and returned.
func ParseConfig(path string) (*Config, error) {
	config, err := GetConfig(path)
	if err != nil {
		if perr, ok := err.(*os.PathError); ok {
			fmt.Printf("config file %q invalid: %s\n", perr.Path, perr.Err)
			return nil, perr
		}

		fmt.Println(err)
		return nil, err
	}

	return config, nil
}
