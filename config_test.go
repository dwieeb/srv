package srv

import (
	"io/ioutil"
	"testing"
)

func TestUnmarshalSampleConfig(t *testing.T) {
	b, err := ioutil.ReadFile("config.sample.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	c, err := UnmarshalConfig(b)
	if err != nil {
		t.Error(err)
		t.Fail()
	}

	if c.Addsite.VirtualHostDir != "/etc/apache2/sites-available" {
		t.Errorf("%v was not expected", c.Addsite.VirtualHostDir)
		t.Fail()
	}

	if c.Addsite.SiteDir != "/home/{{ .Username }}/{{ .Domain }}" {
		t.Errorf("%v was not expected", c.Addsite.SiteDir)
		t.Fail()
	}
}

func TestGetConfigValue(t *testing.T) {
	c, err := GetConfig("config.sample.json")
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	type S struct {
		Username string
		Domain   string
	}

	v, err := GetConfigValue(c.Addsite.SiteDir, S{"a", "b"})
	if err != nil {
		t.Error(err)
		t.FailNow()
	}

	if v != "/home/a/b" {
		t.Errorf("%v was not expected", v)
		t.Fail()
	}
}
