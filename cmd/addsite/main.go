package main

import (
	"bytes"
	"flag"
	"fmt"
	"io"
	"os"
	"path/filepath"
	"text/template"

	"gitlab.com/dwieeb/srv"
)

var (
	flags  = flag.NewFlagSet("main", flag.ExitOnError)
	stdout *bool
	conf   *string
)

type addSiteData struct {
	Username string
	Domain   string
}

type fileData struct {
	path    string
	tmpl    *template.Template
	buffer  io.ReadWriter
	channel chan error
}

func (f *fileData) MakeBuffer() (io.ReadWriter, error) {
	if *stdout {
		return bytes.NewBuffer([]byte{}), nil
	}

	err := os.MkdirAll(filepath.Dir(f.path), srv.Settings.FileMode)
	if err != nil {
		return nil, err
	}

	return os.Create(f.path)
}

func init() {
	flags.Usage = func() {
		fmt.Fprintf(os.Stderr, "Usage: %s [options] <domain> <username>\n", os.Args[0])
		flags.PrintDefaults()
	}

	conf = flags.String("conf", srv.Settings.ConfigPath, "read config from `path`")
	stdout = flags.Bool("stdout", false, "use stdout instead of writing files")
}

func writeSiteFile(file fileData, data addSiteData) {
	var err error

	if *stdout {
		err = srv.WriteFileDisplayHeader(file.buffer, file.path)
		if err != nil {
			fmt.Println(err)
			file.channel <- err
		}
	}

	if *stdout && file.tmpl == srv.EmptyTemplate {
		_, err = file.buffer.Write([]byte("(empty)\n"))
	} else {
		err = srv.WriteTemplatedFile(file.buffer, file.tmpl, data)
	}
	if err != nil {
		fmt.Println(err)
		file.channel <- err
	}

	file.channel <- nil
}

func run(config *srv.Config, data addSiteData) error {
	vhostDir, err := srv.GetConfigValue(config.Addsite.VirtualHostDir, data)
	if err != nil {
		fmt.Println(err)
		return err
	}

	siteDir, err := srv.GetConfigValue(config.Addsite.SiteDir, data)
	if err != nil {
		fmt.Println(err)
		return err
	}

	files := []fileData{
		fileData{
			path:    fmt.Sprintf("%s/%s.conf", vhostDir, data.Domain),
			tmpl:    srv.VirtualHostTemplate,
			channel: make(chan error),
		},
		fileData{
			path:    fmt.Sprintf("%s/public_html/index.html", siteDir),
			tmpl:    srv.IndexHTMLTemplate,
			channel: make(chan error),
		},
		fileData{
			path:    fmt.Sprintf("%s/public_html/favicon.ico", siteDir),
			tmpl:    srv.EmptyTemplate,
			channel: make(chan error),
		},
	}

	for i := range files {
		files[i].buffer, err = files[i].MakeBuffer()
		if err != nil {
			fmt.Println(err)
			return err
		}
	}

	if *stdout {
		fmt.Printf("%s will commit %d files (without the --stdout flag):\n", os.Args[0], len(files))
	}

	for i := range files {
		go writeSiteFile(files[i], data)
	}

	for i := range files {
		if err := <-files[i].channel; err != nil {
			fmt.Println(err)
			return err
		}
	}

	if *stdout {
		for i := range files {
			_, err = io.Copy(os.Stdout, files[i].buffer)
			if err != nil {
				fmt.Println(err)
				return err
			}
		}
	}

	return nil
}

func main() {
	var err error

	err = srv.ParseFlags(flags, 2)
	if err != nil {
		os.Exit(2)
	}

	config, err := srv.ParseConfig(*conf)
	if err != nil {
		os.Exit(1)
	}

	domain := flags.Arg(0)
	username := flags.Arg(1)
	data := addSiteData{username, domain}

	err = run(config, data)
	if err != nil {
		os.Exit(1)
	}

	if !*stdout {
		fmt.Printf("Site %s added.\n", data.Domain)
		fmt.Println("To enable the site, run:")
		fmt.Printf("    a2ensite %s.conf\n", data.Domain)
	}
}
