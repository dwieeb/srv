package srv

import "os"

type settings struct {
	ConfigPath string
	FileMode   os.FileMode
}

// Settings are common settings for these commands.
var Settings = settings{
	ConfigPath: "/etc/srv/config.json",
	FileMode:   0775,
}
