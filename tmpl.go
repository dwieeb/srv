package srv

import (
	"io"
	"strings"
	"text/template"
)

// WriteTemplatedFile will write the templated file to w.
func WriteTemplatedFile(w io.Writer, t *template.Template, data interface{}) error {
	return t.Execute(w, data)
}

// WriteFileDisplayHeader will write the file path to w.
func WriteFileDisplayHeader(w io.Writer, path string) error {
	s := "\n" + path + "\n" + strings.Repeat("-", len(path)) + "\n"
	_, err := w.Write([]byte(s))
	return err
}

// EmptyTemplate is the zero value of a template.
var EmptyTemplate = template.Must(template.New("empty").Parse(""))

const virtualHostText = `<VirtualHost *:80>
    <IfModule mpm_itk_module>
        AssignUserId {{ .Username }} {{ .Username }}
    </IfModule>

    ServerName www.{{ .Domain }}
    ServerAlias {{ .Domain }} *.{{ .Domain }}

    DocumentRoot /home/{{ .Username }}/{{ .Domain }}/public_html
    <Directory />
        Options -Indexes +FollowSymLinks
        AllowOverride All
        Require all granted
    </Directory>

    ErrorLog /home/{{ .Username }}/{{ .Domain }}/logs/error.log

    # Possible values include: debug, info, notice, warn, error, crit,
    # alert, emerg.
    LogLevel warn

    CustomLog /home/{{ .Username }}/{{ .Domain }}/logs/access.log combined
</VirtualHost>
`

// VirtualHostTemplate is the parsed template for Apache2 virtual host files.
var VirtualHostTemplate = template.Must(template.New("VirtualHost").Parse(virtualHostText))

const indexHTMLText = `<!doctype html>
<html>
    <head>
        <title>Hello World!</title>
    </head>
    <body>
        <h1>Future home of {{ .Domain }}</h1>
        <p>This is a placeholder for a new site coming soon!</p>
    </body>
</html>
`

// IndexHTMLTemplate is the parsed template for the placeholder index.html files.
var IndexHTMLTemplate = template.Must(template.New("IndexHTML").Parse(indexHTMLText))
